const UserPersonalDatas = [
    {
        id: 1,
        firstname: "Blas",
        lastname: "Pierre-Gérard",
        job_title_01: "Développeur web Fullstack",
        job_title_02: "Webdesigner UI/UX",
        job_title_03: "Passionné & créatif",
        job_title_04: "Autodidacte",
        job_title_05: "Proactif",
        adress: "36, Boulevard Vauban - 62400",
        city: "Béthune",
        country: "France",
        age: "37 ans",
        phone: "06 59 74 83 03",
        email: "contact@interstellab.com",
        emailmailto: "mailto:"+"contact@interstellab.com",
        siteurltext: "www.interstellab.com",
        siteurl: "https://www.interstellab.com/",
        idlinkedin: "pg-blas-68aa91190",
        linkedinurl: "https://www.linkedin.com/in/pg-blas-68aa91190/",
        gitlaburl: "https://gitlab.com/pgb_oc",
        bio: "Développeur web frontend / full stack. Passionné depuis mon plus jeune âge, je me suis formé en autodidacte dans divers langages informatiques (Javascript, PHP, NodeJS, ReactJs…). Actuellement j'offre mes services en freelance/remote dans le domaine de la création de site internet et le développement d'application Frontend/Backend.  Concernant ma veille technologique, j'ai validé une certification RNCP OpenClassRooms en Janvier 2022. Polyvalent, je maîtrise les différentes étapes techniques de la création d’un site ou d’une application web ; de la compréhension des besoins utilisateurs, au développement frontend/backend et la maintenance.",
        job_title: [
          {
            id: 1,
            job_title: "Développeur web Fullstack",
          },
          {
            id: 2,
            job_title: "Autodidacte",
          },
          {
            id: 3,
            job_title: "Webdesigner UI/UX",
          },
          {
            id: 4,
            job_title: "Passionné & créatif",
          },
          {
            id: 5,
            job_title: "Proactif",
          },
        ],
        
        language: [
            {
              id: 1,
              title: "Anglais",
              rating: "4",
            },
            {
              id: 2,
              title: "Espagnol",
              rating: "3",
            },
            {
              id: 3,
              title: "Goaould",
              rating: "5",
            },
          ],
          interest: [
            {
              id: 1,
              title: "Informatique",
              iconname: "MusicNoteIcon",
            },
            {
              id: 2,
              title: "Musique",
              iconname: "HomeWorkIcon",
            },
            {
              id: 3,
              title: "Auto-Construction",
              iconname: "SkateboardingIcon",
            },
            {
              id: 4,
              title: "Sport",
              iconname: "SportsEsportsRoundedIcon",
            },
            {
              id: 5,
              title: "Jeux-vidéo",
              iconname: "MusicNoteIcon",
            },
          ],
    },
]

export default UserPersonalDatas;