const userData = [
    {
        id: 1,
        title: "Formation Développeur Web OpenClassRooms",
        date: "04/2021 - 12/2021",
        location: "Béthune - Formation à distance",
        certification: "Titre RNCP Développeur intégrateur en réalisation d'applications web - Niveau Bac+2",
    },
    {
        id: 2,
        title: "BEP Comptabilité",
        date: "04/2021 - 12/2021",
        location: "Béthune - Lycée Malhraux",
        certification: "Brevêt d'étude professionnel",
    },
    {
        id: 3,
        title: "sqdSQDQsd",
        date: "04/2021 - 12/2021",
        location: "Béthune - Lycée Malhraux",
        certification: "Brevêt d'étude professionnel",
    },
]

export default userData;