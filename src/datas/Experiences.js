export const userExpData = [
  {
    id: 1,
    title: "Développeur Web - Full Stack",
    date: "12/03/2017 - 21/08/2020",
    location: "Paris 13ème",
    text: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam facilis at iure culpa inventore consequatur.`,
    missions: [
      {
        id: 1,
        title: "mission 1",
      },
      {
        id: 2,
        title: "mission 1",
      },
      {
        id: 3,
        title: "mission 1",
      },
    ],
  },
  {
    id: 2,
    title: "Développeur Web",
    date: "23/01/2012 - 05/06/2016",
    location: "Montpellier",
    text: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam facilis at iure culpa inventore consequatur.`,
    missions: [
      {
        id: 1,
        title: "mission 1",
      },
      {
        id: 2,
        title: "mission 1",
      },
      {
        id: 3,
        title: "mission 1",
      },
    ],
  },
  {
    id: 3,
    title: "LALEMANT SARL",
    date: "2009 à 2014",
    location: "Béthune",
    text: `Employé de bureau`,
    missions: [
      {
        id: 1,
        title: "Negoce fret",
      },
      {
        id: 2,
        title: "comptablité",
      },
      {
        id: 3,
        title: "relation clients",
      },
    ],
  },
  {
    id: 4,
    title: "LALEMANT SARL",
    date: "2009 à 2014",
    location: "Béthune",
    text: `Employé de bureau`,
    missions: [
      {
        id: 1,
        title: "Negoce fret",
      },
      {
        id: 2,
        title: "comptablité",
      },
      {
        id: 3,
        title: "relation clients",
      },
    ],
  },
]
export default userExpData