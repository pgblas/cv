import React, { useState, useContext } from 'react';
import './App.css';
import './sass/css/app.css';
import './sass/css/styles.css';
import User from "./components/User";
import Skills from "./components/Skills";
import Profil from "./components/Profil";
import Formations from "./components/Formations";
import Footer from "./components/Footer";
import DarkMode from "./components/DarkMode";
import SocialIcons from "./components/SocialIcons";
import Date from "./components/Date";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import { Preview, print } from "react-html2pdf";
import './fontawesome';
import UserPersonalDatas from "./datas/User";
import UserSkillData from "./datas/Skills";
import LangueSwitch from "./components/LangueSwitch";
import { Suspense } from 'react';
//import { ExampleComponent } from "./components/ExampleComponent";


function App(props) {
  const [language, setLanguage] = useState("");

  const handleGenerateCv = () => {
    let cvTemplate = document.getElementById("cv-print")
    cvTemplate.setAttribute("style", "width:210mm !important", "height:290mm !important")
    cvTemplate.classList.add("cv-print")
    document.body.classList.remove("dark")
    setTimeout(() => {
      print("CV_BlasPG", "cv-print")
      cvTemplate.setAttribute("style", "width:100% !important", "height:100% !important")
      cvTemplate.classList.remove("cv-print")
    }, 300)
  }
  return (
    <Suspense fallback="...is loading">
      <Preview id={"cv-print"} className="cv-print">
        <div className="App">
          <div className="grid__container">
            <div className="sidebar">
              <div className="sidebar-header">
                <div className="actions appear-animation fadeinup">
                  <DarkMode className="icon icon-rounded" />
                  <button onClick={handleGenerateCv}>
                    <PictureAsPdfIcon className="icon icon-rounded appear-animation fadeinup" />
                  </button>
                    <div className="social appear-animation fadeinup">
                    <SocialIcons />
                    </div>
                </div>
                <LangueSwitch />
              </div>
              <User lang={language} />
              <Skills datas={UserSkillData} />
            </div>
            <div className="main">
              {/*<ExampleComponent lang={language} />*/}
              <Profil datas={UserPersonalDatas} lang={language} />
              <Formations />
              <Footer />
              <Date lang={language} />
            </div>
          </div>
        </div>
      </Preview>
    </Suspense>
  );
}

export default App;