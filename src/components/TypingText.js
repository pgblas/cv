

import React, { useState } from 'react';
import { useTranslation } from "react-i18next";
import TypeAnimation from 'react-type-animation';
import WorkIcon from '@material-ui/icons/Work';

function TypingText(props) {
    const TYPING_PAUSE_MS = 3000
    const { t } = useTranslation();
    const [language, setLanguage] = useState("");
    return (<>
        <div>
            <WorkIcon className="icon-typing icon icon-circle appear-animation fadeinup" />
        </div>
        <div className="typing-container" style={{ width: '28em' }} >
            <div className="typing-text-container">
                <TypeAnimation
                    cursor={true}
                    sequence={[
                        t("job_title_01"),
                        TYPING_PAUSE_MS,
                        t("job_title_02"),
                        TYPING_PAUSE_MS,
                        t("job_title_03"),
                        TYPING_PAUSE_MS,
                        t("job_title_04"),
                        TYPING_PAUSE_MS,
                        t("job_title_05"),
                        TYPING_PAUSE_MS,
                    ]}
                    wrapper="div"
                    repeat={Infinity}
                    lang={language}
                    key={t("id")}
                />
            </div>
        </div>
    </>);
} [];

export default TypingText;

