import {getCurrentDate} from './utils/currentDate'
import { useTranslation } from "react-i18next";
import "../translations/i18n";

function Date(props) {
  const { t } = useTranslation('', { keyPrefix: '' });
    return (<div className="date">
          <p className="update-date">{t("footer_update_date")} {getCurrentDate()}{/*t("date_format", { date: new Date() })} ={" "*/}</p>
        </div>);
  }

export default Date;
