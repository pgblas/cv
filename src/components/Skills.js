import React, { useState } from 'react';
//import userSkillData from "../datas/Skills";
import UserPersonalDatas from "../datas/User";
import SkillsSideBar from './SkillsSideBar';
import SkillsLanguages from './SkillsLanguages';
//import SkillsProgressBar from './SkillsProgressBar';
import Interests from './Interests';

function Skills(props) {
  const [language] = useState("fr");
    return (
      <>
        <SkillsSideBar lang={language}/>
        <SkillsLanguages lang={language} />
        <Interests datas={UserPersonalDatas} lang={language} />
        {/*<SkillsProgressBar datas={userSkillData} />*/}
      </>
    )
  }

export default Skills;