          {experiences.map(({ id, title, url, date, location, text, mission }) => (
              <div className="grid__row" key={id}>
              <div className="grid__item">
                <h3 className="grid__title">{title}</h3>
                <li className="grid__date">{title === " " ? " " : " "} {date}<span className="grid__location">{title === " " ? " " : "/"} {location}</span></li>
                <p className="grid__text"><a href={url} target="_blank" title={text}>{text}</a></p>
                <ul className="grid__missions">
                  {mission.map(mission => {
                    if (mission.id !== " ") {
                      if (mission.title) {
                        return <li className="grid__mission" key={mission.id}>
                          <ArrowForwardIosIcon className="ArrowForwardIosIcon__small" />
                          <a href={mission.url} target="_blank" title={mission.title}>
                            {mission.title === " " ? " " : `${mission.title}`}
                          </a>
                        </li>
                      }
                      else {
                        return null;
                      }
                    }
                  })}
                </ul>
              </div>
            </div>
          ))}