import React from 'react';
import { useTranslation } from "react-i18next";

function Profil(props) {
  const { t } = useTranslation('', { keyPrefix: '' });
  let lng = props.lang;

  const users = [
    {
      id: t(`user.0.id`),
      bio: t(`user.0.bio`),
    }
  ]

  return <div className="profil" style={{ textAlign: lng === "arab" ? "right" : "left" }}>
    <h2 className="h2 title-banner"><span className="underline"></span>{t("h2_title_profil")}</h2>
    {users.map(({ id, bio }) => {
      if (id !== " ") {
        return <p className="profil__text" key={id}>{bio}</p>
      }
    })}
  </div>;
}

export default Profil;

