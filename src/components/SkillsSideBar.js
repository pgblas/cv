import React, { useState } from 'react';
//import UserPersonalDatas from "../datas/User";
import SkillsProgressCircle from "./SkillsProgressCircle"
//import SkillsProgressBar from "./SkillsProgressBar"
import SkillsColumnTwo from './SkillsColumnTwo'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from "react-i18next";

function Skills(props) {
    const { t } = useTranslation('', { keyPrefix: '' });
    const [language] = useState("fr");
    const skills = [
        {
            id: t('skill.0.id'),
            idbar: t('skill.0.idbar'),
            name: t('skill.0.name'),
            progress: t('skill.0.progress'),
            rating: t('skill.0.rating'),
            faicon: t('skill.0.faicon'),
          },
          {
            id: t('skill.1.id'),
            idbar: t('skill.1.idbar'),
            name: t('skill.1.name'),
            progress: t('skill.1.progress'),
            rating: t('skill.1.rating'),
            faicon: t('skill.1.faicon'),
          },
          {
            id: t('skill.2.id'),
            idbar: t('skill.2.idbar'),
            name: t('skill.2.name'),
            progress: t('skill.2.progress'),
            rating: t('skill.2.rating'),
            faicon: t('skill.2.faicon'),
          },
          {
            id: t('skill.3.id'),
            idbar: t('skill.3.idbar'),
            name: t('skill.3.name'),
            progress: t('skill.3.progress'),
            rating: t('skill.3.rating'),
            faicon: t('skill.3.faicon'),
          },
          {
            id: t('skill.4.id'),
            idbar: t('skill.4.idbar'),
            name: t('skill.4.name'),
            progress: t('skill.4.progress'),
            rating: t('skill.4.rating'),
            faicon: t('skill.4.faicon'),
          },
          {
            id: t('skill.5.id'),
            idbar: t('skill.5.idbar'),
            name: t('skill.5.name'),
            progress: t('skill.5.progress'),
            rating: t('skill.5.rating'),
            faicon: t('skill.5.faicon'),
          },
          {
            id: t('skill.6.id'),
            idbar: t('skill.6.idbar'),
            name: t('skill.6.name'),
            progress: t('skill.6.progress'),
            rating: t('skill.6.rating'),
            faicon: t('skill.6.faicon'),
          },
          {
            id: t('skill.7.id'),
            idbar: t('skill.7.idbar'),
            name: t('skill.7.name'),
            progress: t('skill.7.progress'),
            rating: t('skill.7.rating'),
            faicon: t('skill.7.faicon'),
          },
          {
            id: t('skill.8.id'),
            idbar: t('skill.8.idbar'),
            name: t('skill.8.name'),
            progress: t('skill.8.progress'),
            rating: t('skill.8.rating'),
            faicon: t('skill.8.faicon'),
          },
          {
            id: t('skill.9.id'),
            idbar: t('skill.9.idbar'),
            name: t('skill.9.name'),
            progress: t('skill.9.progress'),
            rating: t('skill.9.rating'),
            faicon: t('skill.9.faicon'),
          },
      ]

    return (
        <>
            <div className="skills">
                <h2 className="skills__h2"><span className="underline"></span>{t("h2_title_skills")}</h2>
                {skills.map(({ id, idbar, name, progress, rating, faicon }) => (
                    <div className="skill__text"  key={id}><FontAwesomeIcon icon={faicon}/> <span className="skill__title">{name}</span>
                        {/*<SkillsProgressCircle text={name} rating={rating} />*/}
                        <div className="skill" onLoad={() => moveProgressBar()}>
                            <div className="progress__wrap progress progress__limit" key={idbar} progress={progress}>
                                <div className="progress__bar progress"></div>
                            </div>
                        </div>
                    </div>
                ))}
                <SkillsColumnTwo lang={language}/>
            </div>
        </>
    )

}

export default Skills;