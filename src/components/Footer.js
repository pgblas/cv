import React from 'react';
import FavoriteIcon from '@material-ui/icons/Favorite';
import CopyrightIcon from '@mui/icons-material/Copyright';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from "react-i18next";

function Footer(props) {
  const { t } = useTranslation('', { keyPrefix: '' });

  return <footer className="footer">
      <div className="copyright">
        <p className="author print__date"><span className="print-made-with">{t("footer_made_with")}</span><FavoriteIcon id="heart" /><span className="print-made-end-hide">{t("footer_made_and")}</span> <FontAwesomeIcon id="react" icon={["fab", "react"]} spin />  <span className="print-made-by">{t("footer_made_by")}</span> {t("firstname")} {t("lastname")} - <CopyrightIcon className="icon icon-circle icon-small" /> {t("footer_copyright_date")}</p>
      </div>
  </footer>;
}

export default Footer;
