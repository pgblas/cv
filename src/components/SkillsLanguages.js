import React, { useState } from 'react';
import SkillsProgressCircle from "./SkillsProgressCircle";
import { useTranslation } from "react-i18next";

function SkillsLanguages(props) {
    const [language, setLanguage] = useState("fr");
    const { t } = useTranslation('', { keyPrefix: '' });

    const languages = [
        {
            id: t('language.0.id'),
            title: t('language.0.title'),
            rating: t('language.0.rating'),
        },
        {
            id: t('language.1.id'),
            title: t('language.1.title'),
            rating: t('language.1.rating'),
        },
        {
            id: t('language.2.id'),
            title: t('language.2.title'),
            rating: t('language.2.rating'),
        },
    ]

    return (
        <>
            <h2 className="skills__lang"><span className="underline"></span>{t("h2_title_lang")}</h2>
            <>
                <div className="skills">
                    <div>
                        {languages.map(({ id, title, rating }) => (
                            <span className="skill__text" key={id}>{title}
                                {rating === " " ? " " : (<SkillsProgressCircle title={title} rating={rating} />)}
                            </span>
                        ))}
                    </div>
                </div>
            </>
        </>
    )
}

export default SkillsLanguages;
