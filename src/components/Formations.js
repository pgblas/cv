import React, { useState, useContext } from 'react';
import userData from "../datas/Formations"
import userExpData from "../datas/Experiences"
import Forma from "./Forma"
import Experiences from "./Experiences"

function Formations() {
  const [language, setLanguage] = useState("fr");
    return (
      <>
        <Forma datas={userData} lang={language} />
        <Experiences datas={userExpData} lang={language} />
      </>
    )
  }

export default Formations;
