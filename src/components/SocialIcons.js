import React from 'react';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import GitHubIcon from '@mui/icons-material/GitHub';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from "react-i18next";

function SocialIcons(props) {
  const { t } = useTranslation('', { keyPrefix: '' });
  const users = [
    {
      id: t(`user.0.id`),
      linkedin_id: t(`user.0.linkedin_id`),
      fb_url_social_icon: t(`user.0.fb_url_social_icon`),
      linkedin_url_social_icon: t(`user.0.linkedin_url_social_icon`),
      gitlab_url_social_icon: t(`user.0.gitlab_url_social_icon`),
      github_url_social_icon: t(`user.0.github_url_social_icon`),
    }
  ]
  return <>
    {users.map(({ id, fb_url_social_icon, linkedin_url_social_icon, gitlab_url_social_icon, github_url_social_icon }) => (
        <div className="social-icons" key={id}>
        {fb_url_social_icon === " " ? " " : (<a href={fb_url_social_icon}><FacebookIcon href={fb_url_social_icon} className="icon icon-rounded" /></a>)}
        {linkedin_url_social_icon === " " ? " " : (<a href={linkedin_url_social_icon}><LinkedInIcon href={linkedin_url_social_icon} className="icon icon-rounded" /></a>)}
        {gitlab_url_social_icon === " " ? " " : (<a href={gitlab_url_social_icon}><FontAwesomeIcon icon="fa-brands fa-gitlab" href={gitlab_url_social_icon} className="icon icon-fa icon-rounded" /></a>)}
        {github_url_social_icon === " " ? " " : (<a href={github_url_social_icon}><GitHubIcon href={github_url_social_icon} className="icon icon-rounded" /></a>)}
        </div> 
    ))}
  </>;
}

export default SocialIcons;
