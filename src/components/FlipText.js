import React from 'react';
//import WorkIcon from '@material-ui/icons/Work';
import { useTranslation } from "react-i18next";

function FlipText(props) {
    const { t } = useTranslation('', { keyPrefix: '' });
    const jobs = [
        {
            id: t('job.0.id'),
            title: t('job.0.title'),
        },
        {
            id: t('job.1.id'),
            title: t('job.1.title'),
        },
        {
            id: t('job.2.id'),
            title: t('job.2.title'),
        },
        {
            id: t('job.3.id'),
            title: t('job.3.title'),
        },
        {
            id: t('job.4.id'),
            title: t('job.4.title'),
        },
    ]
    return <> 
        <div className="flip-text-container">
            {/*<WorkIcon className="icon icon-circle" />*/}
                <div>
                    <div className="flip-text">
                        {jobs.map(({ id, title }) => (
                            <div key={id}><div>{title}</div></div>
                        ))}
                    </div>
                </div>
        </div>
    </>;
}

export default FlipText;