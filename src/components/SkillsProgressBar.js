import React, { useEffect } from 'react'

function SkillProgressBar(props) {
  const datas = props.datas
  useEffect(() => {
    // on page load...
    moveProgressBar();
    // on browser resize...
    $(window).resize(function () {
      moveProgressBar();
    });

  }, [])

  // SIGNATURE PROGRESS
  function moveProgressBar() {
    console.log("moveProgressBar");

    var getPercent = ($('.progress__wrap').attr('progress') / 100);
    var getProgressWrapWidth = $('.progress__wrap').width();
    var progressTotal = getPercent * getProgressWrapWidth;
    var animationLength = 2500;

    // on page load, animate percentage bar to data percentage length
    // .stop() used to prevent animation queueing
    $('.progress__bar').stop().animate({
      left: progressTotal
    }, animationLength);
  }

  return (
  <>
  </>
  )
}

export default SkillProgressBar;