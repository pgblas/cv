
import React from 'react'
import ChatIcon from '@mui/icons-material/Chat';
import EmojiFlagsIcon from '@mui/icons-material/EmojiFlags';
import VideoLibraryIcon from '@mui/icons-material/VideoLibrary';
import ComputerIcon from '@material-ui/icons/Computer';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import SkateboardingIcon from '@mui/icons-material/Skateboarding';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
//import UserPersonalDatas from "../../datas/User";


class Icon extends React.Component {
  
  render() {
    const { type, ...props } = this.props

    let Icon = null
    switch (type) {
      case 'flat': Icon = MusicNoteIcon
      break
      case 'Icon': Icon = HomeWorkIcon
      break
      default: Icon = ChevronRightIcon;
      break
    }

    return (
      React.createElement(Icon, { ...props, disabletouchripple: "true", disablefocusripple: "true" })
    )
  }
}

export default Icon

/*
import * as MuiIcons from '@material-ui/icons';



import React from 'react'

function Icon({ iconName }) {

  return (
    <>
    {[iconName]}
  </>
  )
}

export default Icon
*/