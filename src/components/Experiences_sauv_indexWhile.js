import React from "react"
import { useTranslation } from "react-i18next";

function Experiences(props) {
  const { t } = useTranslation('', { keyPrefix: '' });
  //const datas = props.datas

  for (var index = 0; index < 5; index++) {
    for (var i = 0; i < 5; i++) {
    const experiences = [
      {
        id: t(`experience.${index}.id`),
        title: t(`experience.${index}.title`),
        date: t(`experience.${index}.date`),
        location: t(`experience.${index}.location`),
        text: t(`experience.${index}.text`),
        mission: [
          {
            id: t(`experience.${index}.mission.${i}.id`),
            title: t(`experience.${index}.mission.${i}.title`),
          },
          {
            id: t(`experience.${index}.mission.${i}.id`),
            title: t(`experience.${index}.mission.${i}.title`),
          },
          {
            id: t(`experience.${index}.mission.${i}.id`),
            title: t(`experience.${index}.mission.${i}.title`),
          },
          {
            id: t(`experience.${index}.mission.${i}.id`),
            title: t(`experience.${index}.mission.${i}.title`),
          },
          {
            id: t(`experience.${index}.mission.${i}.id`),
            title: t(`experience.${index}.mission.${i}.title`),
          },    
        ],
      },
    ]

    return <div className="experiences">
      <h2 className="h2 title-banner">{t("h2_title_experiences")}</h2>
      <>
        {experiences.map(({ id, title, date, location, text, mission }) => (
          <div className="grid__row" key={id}>
            <div className="grid__item">
              <h3 className="grid__title">{title}
                <span className="grid__date">{title === " " ? " " : "-"} {date}</span></h3>
              <p className="grid__location">{location}</p>
              <p className="grid__text">{text}</p>
              <ul className="grid__missions">
                {mission.map(mission => {
                  if (mission.title) {
                    return <li className="grid__mission" key={mission.id}>
                      {mission.title}
                    </li>
                  } else {
                    return null
                  }
                })}
              </ul>
            </div>
          </div>
        ))}
      </>
    </div>
    }}
  }

export default Experiences