import React, { useState, useContext } from 'react';
import { useTranslation } from "react-i18next";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FlipText from "./FlipText";
//import TypingText from "./TypingText";
import HomeIcon from '@material-ui/icons/Home';
import PhoneAndroidIcon from '@material-ui/icons/PhoneAndroid';
import MailIcon from '@material-ui/icons/Mail';
//import EventIcon from '@material-ui/icons/Event';
//import LanguageIcon from '@material-ui/icons/Language';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import DateRangeIcon from '@material-ui/icons/DateRange';
import LinkIcon from '@mui/icons-material/Link';
import GitHubIcon from '@mui/icons-material/GitHub';
import PersonIcon from '@mui/icons-material/Person';
//import UserPersonalDatas from "../datas/User";

function User(props) {

    const [language, setLanguage] = useState("");
    const { t } = useTranslation('', { keyPrefix: '' });
    //let lng = props.lang;
    //const datas = props.datas;

    const users = [
        {
            id: t(`user.0.id`),
            avatar_url: t(`user.0.avatar_url`),
            avatar_default_url: t(`user.0.avatar_default_url`),
            firstname: t(`user.0.firstname`),
            lastname: t(`user.0.lastname`),
            adress: t(`user.0.adress`),
            city: t(`user.0.city`),
            country: t(`user.0.country`),
            age: t(`user.0.age`),
            years_old: t(`user.0.years_old`),
            phone: t(`user.0.phone`),
            email: t(`user.0.email`),
            email_mailto: t(`user.0.email_mailto`),
            site_url_text: t(`user.0.site_url_text`),
            site_url: t(`user.0.site_url`),
            site_02_url_text: t(`user.0.site_02_url_text`),
            site_02_url: t(`user.0.site_02_url`),
            fb_url: t(`user.0.fb_url`),
            link_title_01: t(`user.0.link_title_01`),
            link_title_02: t(`user.0.link_title_02`),
            link_title_03: t(`user.0.link_title_03`),
            linkedin_id: t(`user.0.linkedin_id`),
            linkedin_url: t(`user.0.linkedin_url`),
            gitlab_url: t(`user.0.gitlab_url`),
            github_url: t(`user.0.github_url`),
            job_title_01: t(`user.0.job_title_01`),
            link_title_04: t(`user.0.link_title_04`),
            cv_url: t(`user.0.cv_url`),
        }
    ]

    const jobs = [
        {
            id: t(`user.0.id`),
            title: t(`job.0.title`),
        }
    ]

    return <div className="user">   
                <div className="particles-container">
                    <div className="particles">
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                        <div className="particle"></div>
                    </div>
                </div>
                {users.map(({ id, avatar_url, avatar_default_url, firstname, lastname, adress, city, country, age, years_old, phone,
        email, email_mailto, site_url_text, site_url, site_02_url_text, site_02_url, link_title_01, link_title_02, link_title_03, linkedin_id, linkedin_url,
        gitlab_url, github_url, link_title_04, cv_url }) => {
                    if (id !== " ") {
                        return <div key={id}>
                            <div className="avatar-container">
                            {avatar_url === " " ? (<img className="user__avatar" src={avatar_default_url} alt={firstname}/>) : (<img className="user__avatar" src={avatar_url} alt={firstname}/>)}
                            </div>
                            <div className="user__animated-container">
                                <div className="user__animated-circle">
                                    <div>
                                    </div>
                                </div>
                            </div>
                            <h1 className="user__name"><PermIdentityIcon className="icon icon-circle" />{firstname} {lastname}</h1>
                            <div className="user__job media-hide print-show">
                            {jobs.map(({ id, title }) => (
                                <div key={id}>
                                    {title === " " ? " " : (<p className="print__job-title">{title}</p>)}
                                </div>
                                ))}
                            </div>

                            <div className="user__job media-show print-hide">
                                <FlipText lang={language} />
                                {/*<TypingText lang={language} />*/}
                            </div>
                            <div className="user__infos" key={id}>
                                {age === " " ? " " : (<p className="user__info user__age"><DateRangeIcon className="icon icon-circle" />{age} {years_old}</p>)}
                                {adress === " " ? " " : (<p className="user__info user__adress"><HomeIcon className="icon icon-circle" />{adress}</p>)}
                                {city === " " ? " " : (<p className="user__info user__city"><LocationOnIcon className="icon icon-circle" />{city} - {country}</p>)}
                                {phone === " " ? " " : (<p className="user__info user__tel"><a href="tel:+33659748303"><PhoneAndroidIcon className="icon icon-circle" />{phone}</a></p>)}
                                {email === " " ? " " : (<p className="user__info user__mail"><a href={email_mailto} title={email_mailto}><MailIcon className="icon icon-circle" />{email}</a></p>)}
                                {site_url === " " ? " " : (<p className="user__info user__site_url"><a href={site_url} title={site_url} target="_blank"><LinkIcon className="icon icon-circle" />{site_url_text}</a></p>)}
                                {site_url === " " ? " " : (<p className="user__info user__site_url"><a href={site_02_url} title={site_02_url} target="_blank"><LinkIcon className="icon icon-circle" />{site_02_url_text}</a></p>)}
                                {linkedin_url === " " ? " " : (<p className="user__info user__linkedIn"><a href={linkedin_url} title={github_url} target="_blank"><LinkIcon className="icon icon-circle" />{link_title_01} {linkedin_id}</a></p>)}
                                {gitlab_url === " " ? " " : (<p className="user__info user__gitLab"><a href={gitlab_url} title={linkedin_url} target="_blank"><FontAwesomeIcon icon="fa-brands fa-gitlab" className="icon icon-fa icon-circle" />{link_title_02}{gitlab_url}</a></p>)}
                                {github_url === " " ? " " : (<p className="user__info user__gitHub"><a href={github_url} title={github_url} target="_blank"><GitHubIcon href={github_url} className="icon icon-circle" />{link_title_03}{github_url}</a></p>)}
                                {cv_url === " " ? " " : (<p className="user__info user__cv_url"><a href={cv_url}><PersonIcon href={cv_url} className="icon icon-circle" />{link_title_04}{cv_url}</a></p>)}
                            </div>
                            <div>

                            </div>
                        </div>
                    }
                    else return null;
                })}

            </div>
};

export default User;