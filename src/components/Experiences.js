import React from "react"
import { useTranslation } from "react-i18next";
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

function Experiences(props) {
  const { t } = useTranslation('', { keyPrefix: '' });
  //const datas = props.datas

  const experiences = [
    {
      id: t(`experience.0.id`),
      title: t(`experience.0.title`),
      url: t(`experience.0.url`),
      technologies_side: t(`experience.0.technologies_side`),
      technologies: t(`experience.0.technologies`),
      date: t(`experience.0.date`),
      location: t(`experience.0.location`),
      text: t(`experience.0.text`),
      mission: [
        {
          id: t(`experience.0.mission.0.id`),
          title: t(`experience.0.mission.0.title`),
          url: t(`experience.0.mission.0.url`),
          technologies_side: t(`experience.0.mission.0.technologies_side`),
          technologies: t(`experience.0.mission.0.technologies`),
        },
        {
          id: t(`experience.0.mission.1.id`),
          title: t(`experience.0.mission.1.title`),
          url: t(`experience.0.mission.1.url`),
          technologies_side: t(`experience.0.mission.1.technologies_side`),
          technologies: t(`experience.0.mission.1.technologies`),
        },
        {
          id: t(`experience.0.mission.2.id`),
          title: t(`experience.0.mission.2.title`),
          url: t(`experience.0.mission.2.url`),
          technologies_side: t(`experience.0.mission.2.technologies_side`),
          technologies: t(`experience.0.mission.2.technologies`),
        },
        {
          id: t(`experience.0.mission.3.id`),
          title: t(`experience.0.mission.3.title`),
          url: t(`experience.0.mission.3.url`),
          technologies_side: t(`experience.0.mission.3.technologies_side`),
          technologies: t(`experience.0.mission.3.technologies`),
        },
        {
          id: t(`experience.0.mission.4.id`),
          title: t(`experience.0.mission.4.title`),
          url: t(`experience.0.mission.4.url`),
          technologies_side: t(`experience.0.mission.4.technologies_side`),
          technologies: t(`experience.0.mission.4.technologies`),
        },
        {
          id: t(`experience.0.mission.5.id`),
          title: t(`experience.0.mission.5.title`),
          url: t(`experience.0.mission.5.url`),
          technologies_side: t(`experience.0.mission.5.technologies_side`),
          technologies: t(`experience.0.mission.5.technologies`),
        },
        {
          id: t(`experience.0.mission.6.id`),
          title: t(`experience.0.mission.6.title`),
          url: t(`experience.0.mission.6.url`),
          technologies_side: t(`experience.0.mission.6.technologies_side`),
          technologies: t(`experience.0.mission.6.technologies`),
        },
        {
          id: t(`experience.0.mission.7.id`),
          title: t(`experience.0.mission.7.title`),
          url: t(`experience.0.mission.7.url`),
          technologies_side: t(`experience.0.mission.7.technologies_side`),
          technologies: t(`experience.0.mission.7.technologies`),
        },
        {
          id: t(`experience.0.mission.8.id`),
          title: t(`experience.0.mission.8.title`),
          url: t(`experience.0.mission.8.url`),
          technologies_side: t(`experience.0.mission.8.technologies_side`),
          technologies: t(`experience.0.mission.8.technologies`),
        },
      ],
    },
    {
      id: t(`experience.1.id`),
      title: t(`experience.1.title`),
      url: t(`experience.1.url`),
      technologies_side: t(`experience.1.technologies_side`),
      technologies: t(`experience.1.technologies`),
      date: t(`experience.1.date`),
      location: t(`experience.1.location`),
      text: t(`experience.1.text`),
      mission: [
        {
          id: t(`experience.1.mission.0.id`),
          title: t(`experience.1.mission.0.title`),
          url: t(`experience.1.mission.0.url`),
          technologies: t(`experience.1.mission.0.technologies`),
        },
        {
          id: t(`experience.1.mission.1.id`),
          title: t(`experience.1.mission.1.title`),
          url: t(`experience.1.mission.1.url`),
          technologies: t(`experience.1.mission.1.technologies`),
        },
        {
          id: t(`experience.1.mission.2.id`),
          title: t(`experience.1.mission.2.title`),
          url: t(`experience.1.mission.2.url`),
          technologies: t(`experience.1.mission.2.technologies`),
        },
        {
          id: t(`experience.1.mission.3.id`),
          title: t(`experience.1.mission.3.title`),
          url: t(`experience.1.mission.3.url`),
          technologies: t(`experience.1.mission.3.technologies`),
        },
        {
          id: t(`experience.1.mission.4.id`),
          title: t(`experience.1.mission.4.title`),
          url: t(`experience.1.mission.4.url`),
          technologies: t(`experience.1.mission.4.technologies`),
        },
        {
          id: t(`experience.1.mission.5.id`),
          title: t(`experience.1.mission.5.title`),
          url: t(`experience.1.mission.5.url`),
          technologies: t(`experience.1.mission.5.technologies`),
        },
        {
          id: t(`experience.1.mission.6.id`),
          title: t(`experience.1.mission.6.title`),
          url: t(`experience.1.mission.6.url`),
          technologies: t(`experience.1.mission.6.technologies`),
        },
        {
          id: t(`experience.1.mission.7.id`),
          title: t(`experience.1.mission.7.title`),
          url: t(`experience.1.mission.7.url`),
          technologies: t(`experience.1.mission.7.technologies`),
        },
        {
          id: t(`experience.1.mission.8.id`),
          title: t(`experience.1.mission.8.title`),
          url: t(`experience.1.mission.8.url`),
          technologies: t(`experience.1.mission.8.technologies`),
        },
      ],
    },
    {
      id: t(`experience.2.id`),
      title: t(`experience.2.title`),
      url: t(`experience.2.url`),
      technologies_side: t(`experience.2.technologies_side`),
      technologies: t(`experience.2.technologies`),
      date: t(`experience.2.date`),
      location: t(`experience.2.location`),
      text: t(`experience.2.text`),
      mission: [
        {
          id: t(`experience.2.mission.0.id`),
          title: t(`experience.2.mission.0.title`),
          url: t(`experience.2.mission.0.url`),
          technologies: t(`experience.2.mission.0.technologies`),
        },
        {
          id: t(`experience.2.mission.1.id`),
          title: t(`experience.2.mission.1.title`),
          url: t(`experience.2.mission.1.url`),
          technologies: t(`experience.2.mission.1.technologies`),
        },
        {
          id: t(`experience.2.mission.2.id`),
          title: t(`experience.2.mission.2.title`),
          url: t(`experience.2.mission.2.url`),
          technologies: t(`experience.2.mission.2.technologies`),
        },
        {
          id: t(`experience.2.mission.3.id`),
          title: t(`experience.2.mission.3.title`),
          url: t(`experience.2.mission.3.url`),
          technologies: t(`experience.2.mission.3.technologies`),
        },
        {
          id: t(`experience.2.mission.4.id`),
          title: t(`experience.2.mission.4.title`),
          url: t(`experience.2.mission.4.url`),
          technologies: t(`experience.2.mission.4.technologies`),
        },
        {
          id: t(`experience.2.mission.5.id`),
          title: t(`experience.2.mission.5.title`),
          url: t(`experience.2.mission.5.url`),
          technologies: t(`experience.2.mission.5.technologies`),
        },
        {
          id: t(`experience.2.mission.6.id`),
          title: t(`experience.2.mission.6.title`),
          url: t(`experience.2.mission.6.url`),
          technologies: t(`experience.2.mission.6.technologies`),
        },
        {
          id: t(`experience.2.mission.7.id`),
          title: t(`experience.2.mission.7.title`),
          url: t(`experience.2.mission.7.url`),
          technologies: t(`experience.2.mission.7.technologies`),
        },
        {
          id: t(`experience.2.mission.8.id`),
          title: t(`experience.2.mission.8.title`),
          url: t(`experience.2.mission.8.url`),
          technologies: t(`experience.2.mission.8.technologies`),
        },
        {
          id: t(`experience.2.mission.9.id`),
          title: t(`experience.2.mission.9.title`),
          url: t(`experience.2.mission.9.url`),
          technologies: t(`experience.2.mission.9.technologies`),
        },
      ],
    },
    {
      id: t(`experience.3.id`),
      title: t(`experience.3.title`),
      url: t(`experience.3.url`),
      technologies_side: t(`experience.3.technologies_side`),
      technologies: t(`experience.3.technologies`),
      date: t(`experience.3.date`),
      location: t(`experience.3.location`),
      text: t(`experience.3.text`),
      mission: [
        {
          id: t(`experience.3.mission.0.id`),
          title: t(`experience.3.mission.0.title`),
          url: t(`experience.3.mission.0.url`),
          technologies: t(`experience.3.mission.0.technologies`),
        },
        {
          id: t(`experience.3.mission.1.id`),
          title: t(`experience.3.mission.1.title`),
          url: t(`experience.3.mission.1.url`),
          technologies: t(`experience.3.mission.1.technologies`),
        },
        {
          id: t(`experience.3.mission.2.id`),
          title: t(`experience.3.mission.2.title`),
          url: t(`experience.3.mission.2.url`),
          technologies: t(`experience.3.mission.2.technologies`),
        },
        {
          id: t(`experience.3.mission.3.id`),
          title: t(`experience.3.mission.3.title`),
          url: t(`experience.3.mission.3.url`),
          technologies: t(`experience.3.mission.3.technologies`),
        },
        {
          id: t(`experience.3.mission.4.id`),
          title: t(`experience.3.mission.4.title`),
          url: t(`experience.3.mission.4.url`),
          technologies: t(`experience.3.mission.4.technologies`),
        },
        {
          id: t(`experience.3.mission.5.id`),
          title: t(`experience.3.mission.5.title`),
          url: t(`experience.3.mission.5.url`),
          technologies: t(`experience.3.mission.5.technologies`),
        },
        {
          id: t(`experience.3.mission.6.id`),
          title: t(`experience.3.mission.6.title`),
          url: t(`experience.3.mission.6.url`),
          technologies: t(`experience.3.mission.6.technologies`),
        },
        {
          id: t(`experience.3.mission.7.id`),
          title: t(`experience.3.mission.7.title`),
          url: t(`experience.3.mission.7.url`),
          technologies: t(`experience.3.mission.7.technologies`),

        },
        {
          id: t(`experience.3.mission.8.id`),
          title: t(`experience.3.mission.8.title`),
          url: t(`experience.3.mission.8.url`),
          technologies: t(`experience.3.mission.8.technologies`),
        },
        {
          id: t(`experience.3.mission.9.id`),
          title: t(`experience.3.mission.9.title`),
          url: t(`experience.3.mission.9.url`),
          technologies: t(`experience.3.mission.9.technologies`),
        },
      ],
    },
    {
      id: t(`experience.4.id`),
      title: t(`experience.4.title`),
      url: t(`experience.4.url`),
      technologies_side: t(`experience.4.technologies_side`),
      technologies: t(`experience.4.technologies`),
      date: t(`experience.4.date`),
      location: t(`experience.4.location`),
      text: t(`experience.4.text`),
      mission: [
        {
          id: t(`experience.4.mission.0.id`),
          title: t(`experience.4.mission.0.title`),
          url: t(`experience.4.mission.0.url`),
          technologies: t(`experience.4.mission.0.technologies`),
        },
        {
          id: t(`experience.4.mission.1.id`),
          title: t(`experience.4.mission.1.title`),
          url: t(`experience.4.mission.1.url`),
          technologies: t(`experience.4.mission.1.technologies`),
        },
        {
          id: t(`experience.4.mission.2.id`),
          title: t(`experience.4.mission.2.title`),
          url: t(`experience.4.mission.2.url`),
          technologies: t(`experience.4.mission.2.technologies`),
        },
        {
          id: t(`experience.4.mission.3.id`),
          title: t(`experience.4.mission.3.title`),
          url: t(`experience.4.mission.3.url`),
          technologies: t(`experience.4.mission.3.technologies`),
        },
        {
          id: t(`experience.4.mission.4.id`),
          title: t(`experience.4.mission.4.title`),
          url: t(`experience.4.mission.4.url`),
          technologies: t(`experience.4.mission.4.technologies`),
        },
        {
          id: t(`experience.4.mission.5.id`),
          title: t(`experience.4.mission.5.title`),
          url: t(`experience.4.mission.5.url`),
          technologies: t(`experience.4.mission.5.technologies`),
        },
        {
          id: t(`experience.4.mission.6.id`),
          title: t(`experience.4.mission.6.title`),
          url: t(`experience.4.mission.6.url`),
          technologies: t(`experience.4.mission.6.technologies`),
        },
        {
          id: t(`experience.4.mission.7.id`),
          title: t(`experience.4.mission.7.title`),
          url: t(`experience.4.mission.7.url`),
          technologies: t(`experience.4.mission.7.technologies`),
        },
        {
          id: t(`experience.4.mission.8.id`),
          title: t(`experience.4.mission.8.title`),
          url: t(`experience.4.mission.8.url`),
          technologies: t(`experience.4.mission.8.technologies`),
        },
        {
          id: t(`experience.4.mission.9.id`),
          title: t(`experience.4.mission.9.title`),
          url: t(`experience.4.mission.9.url`),
          technologies: t(`experience.4.mission.9.technologies`),
        },
      ],
    },
  ]

  return <div className="experiences">
    <h2 className="h2 title-banner"><span className="underline"></span>{t("h2_title_experiences")}</h2>
    {experiences.map(({ id, title, url, technologies_side, technologies, date, location, text, mission }) => {
      if (id !== " " && title && technologies_side && technologies) {
          return <div className="grid__row" key={id}>
            <div className="grid__item">
              <h3 className="grid__title">{title}<span className="grid__date">{title === " " ? " " : " "}  {date}<span className="grid__location">{title === " " ? " " : "/"} {location}</span></span></h3>
              <p className="grid__text">{url === " " ? " " : (<a href={url} target="_blank" title={text}>{text}</a>)}</p>
              <p className="grid__text">{url !== " " ? " " : (text)}</p>
              <ul className="grid__missions">
                {mission.map(mission => {
                  if (mission.title !== " ") {
                    if (mission.title) {
                      return <li className="grid__mission" key={mission.id}>
                        <ArrowForwardIosIcon className="ArrowForwardIosIcon__small" />
                        {mission.url === " " ? " " : (<a className="grid__link" href={mission.url} target="_blank" title={mission.title}>{mission.title === " " ? " " : `${mission.title}`}</a>)}
                        {mission.url !== " " ? " " : (<span>{mission.title === " " ? " " : `${mission.title}`}</span>)}
                        {mission.technologies_side === " " ? " " : (<p className="grid__technologies">{mission.technologies_side} {mission.technologies === " " ? " " : `${mission.technologies}`}</p>)}
                        {mission.url === " " ? " " : (<span className="grid__print-url">{mission.url}</span>)}
                      </li>
                    }
                    else return null;
                  }
                })}
              </ul>
            </div>
          </div>
      }
    })}
  </div>
}
export default Experiences;