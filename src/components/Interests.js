import React from 'react';
import Icon from "./utils/Icons";
import { useTranslation } from "react-i18next";

function Interests(props) {
  const { t } = useTranslation('', { keyPrefix: '' });
  //const datas = props.datas;

  const interests = [
    {
        id: t('interest.0.id'),
        title: t('interest.0.title'),
        iconname: t('interest.0.rating'),
      },
      {
        id: t('interest.1.id'),
        title: t('interest.1.title'),
        iconname: t('interest.1.rating'),
      },
      {
        id: t('interest.2.id'),
        title: t('interest.2.title'),
        iconname: t('interest.2.rating'),
      },
      {
        id: t('interest.3.id'),
        title: t('interest.3.title'),
        iconname: t('interest.3.rating'),
      },
      {
        id: t('interest.4.id'),
        title: t('interest.4.title'),
        iconname: t('interest.4.rating'),
      },
  ]
  
  return <div className="skills">
    <div className="interests">
      <h2 className="h2"><span className="underline"></span>{t("h2_title_interests")}</h2>
        <>
          {interests.map(({ id, title, iconname }) => (
            <ul key={id}>
              <li className="interests__list">             
              <Icon iconname={iconname} className="icon icon-circle icon-small" /><span className="interests__text">{title}</span></li>
            </ul>
          ))}
        </>
    </div>
  </div>;
  
}

export default Interests;