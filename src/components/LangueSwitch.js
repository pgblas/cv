import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { useState } from "react";
import { useTranslation } from "react-i18next";
//import ExampleComponent from "./ExampleComponent";
import {i18n} from "../translations/i18n";
//import { Link } from "gatsby"
import LanguageIcon from '@mui/icons-material/Language';

import FlagFr from '../translations/img/flag/flag-french.svg';
import FlagEn from '../translations/img/flag/flag-english-us.svg';
//import FlagEs from '../translations/img/flag/flag-spanish-mexico.svg';
//import FlagAr from '../translations/img/flag/flag-arabic.svg';
//import FlagMa from '../translations/img/flag/flag-mandarin.svg';

export default function BasicSelect() {
  const [language, setLanguage] = useState("fr");
  const { t } = useTranslation('', { keyPrefix: '' });

  const handleChange = (e) => {
    e.preventDefault();
    setLanguage(e.target.value);
    i18n.changeLanguage(e.target.value);
  };

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth className="languageSwitcher">
        <InputLabel id="international"><LanguageIcon className="icon__lang-select icon-flag icon-globe" /></InputLabel>
        <Select
          labelId="langSelect"
          id="CountrySelect"
          label="Country"
          onChange={handleChange}
          defaultValue="fr"
        >
          <MenuItem value="fr"><img className="icon__lang-select icon-flag" src={FlagFr} /></MenuItem>
          <MenuItem value="en"><img className="icon__lang-select icon-flag" src={FlagEn} /></MenuItem>

        </Select>
      </FormControl>
    </Box>
  );
}

/*
          <MenuItem value="es"><img className="icon__lang-select icon-flag" src={FlagEs} /></MenuItem>
          <MenuItem value="arab"><img className="icon__lang-select icon-flag" src={FlagAr} /></MenuItem>
          <MenuItem value="zh"><img className="icon__lang-select icon-flag" src={FlagMa} /></MenuItem>
*/