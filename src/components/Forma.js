import React from 'react';
import { useTranslation } from "react-i18next";

function Forma(props) {
    const { t } = useTranslation('', { keyPrefix: '' });
    //const datas = props.datas;
    
    const formations = [
      {
          id: t('formation.0.id'),
          title: t('formation.0.title'),
          date: t('formation.0.date'),
          location: t('formation.0.location'),
          certification: t('formation.0.certification'),
          faicon: t('formation.0.faicon'),   
        },
        {
            id: t('formation.1.id'),
            title: t('formation.1.title'),
            date: t('formation.1.date'),
            location: t('formation.1.location'),
            certification: t('formation.1.certification'),
            faicon: t('formation.1.faicon'),
        },
        {
            id: t('formation.2.id'),
            title: t('formation.2.title'),
            date: t('formation.2.date'),
            location: t('formation.2.location'),
            certification: t('formation.2.certification'),
            faicon: t('formation.2.faicon'),
        },
        {
            id: t('formation.3.id'),
            title: t('formation.3.title'),
            date: t('formation.3.date'),
            location: t('formation.3.location'),
            certification: t('formation.3.certification'),
            faicon: t('formation.3.faicon'),
        },
    ]

    return <div className="cursus">
        <h2 className="h2 title-banner"><span className="underline"></span>{t("h2_title_education")}</h2>
        {formations.map(({ id, title, date, location, certification, faicon }) => {
            if (id !== " ") {
                if (title) {
                    return <div className="grid__row" key={id}>
                        <div className="grid__item">
                            <h3 className="grid__title">{title} {title === " " ? " " : "-"} <span className="grid__date">{date}</span><span className="grid__location">{location}</span></h3>
                            <p className="grid__certification">{certification}</p>
                        </div>
                    </div>
                }
            }
            else return null;
        })}
    </div>;
}

export default Forma;