import React from 'react';
//import UserPersonalDatas from "../datas/User";
import SkillsProgressCircle from "./SkillsProgressCircle"
//import SkillsProgressBar from "./SkillsProgressBar"
import CodeIcon from '@mui/icons-material/Code';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from "react-i18next";

function SkillsColumnTwo(props) {
  const { t } = useTranslation('', { keyPrefix: '' });

  const skills = [
    {
      id: t('skill.10.id'),
      idbar: t('skill.10.idbar'),
      name: t('skill.10.name'),
      progress: t('skill.10.progress'),
      rating: t('skill.10.rating'),
      faicon: t('skill.10.faicon'),
    },
    {
      id: t('skill.11.id'),
      idbar: t('skill.11.idbar'),
      name: t('skill.11.name'),
      progress: t('skill.11.progress'),
      rating: t('skill.11.rating'),
      faicon: t('skill.11.faicon'),
    },
    {
      id: t('skill.12.id'),
      idbar: t('skill.12.idbar'),
      name: t('skill.12.name'),
      progress: t('skill.12.progress'),
      rating: t('skill.12.rating'),
      faicon: t('skill.12.faicon'),
    },
    {
      id: t('skill.13.id'),
      idbar: t('skill.13.idbar'),
      name: t('skill.13.name'),
      progress: t('skill.13.progress'),
      rating: t('skill.13.rating'),
      faicon: t('skill.13.faicon'),
    },
    {
      id: t('skill.14.id'),
      idbar: t('skill.14.idbar'),
      name: t('skill.14.name'),
      progress: t('skill.14.progress'),
      rating: t('skill.14.rating'),
      faicon: t('skill.14.faicon'),
    },
    {
      id: t('skill.15.id'),
      idbar: t('skill.15.idbar'),
      name: t('skill.15.name'),
      progress: t('skill.15.progress'),
      rating: t('skill.15.rating'),
      faicon: t('skill.15.faicon'),
    },
    {
      id: t('skill.16.id'),
      idbar: t('skill.16.idbar'),
      name: t('skill.16.name'),
      progress: t('skill.16.progress'),
      rating: t('skill.16.rating'),
      faicon: t('skill.16.faicon'),
    },
    {
      id: t('skill.17.id'),
      idbar: t('skill.17.idbar'),
      name: t('skill.17.name'),
      progress: t('skill.17.progress'),
      rating: t('skill.17.rating'),
      faicon: t('skill.17.faicon'),
    },
    {
      id: t('skill.18.id'),
      idbar: t('skill.18.idbar'),
      name: t('skill.18.name'),
      progress: t('skill.18.progress'),
      rating: t('skill.18.rating'),
      faicon: t('skill.18.faicon'),
    },
    {
      id: t('skill.19.id'),
      idbar: t('skill.19.idbar'),
      name: t('skill.19.name'),
      progress: t('skill.19.progress'),
      rating: t('skill.19.rating'),
      faicon: t('skill.19.faicon'),
    },
  ]

  return (
    <>
      <div className="skills skills_column_two">
        <h2 className="skills__h2">{t("h2_title_skills_column_two")}</h2>
        {skills.map(({ id, idbar, name, progress, rating, faicon }) => (
          <div className="skill__text" key={id}><FontAwesomeIcon icon={faicon} /> <span className="skill__title">{name}</span>
            {/*<SkillsProgressCircle text={name} rating={rating} />*/}
            <div className="skill" onLoad={() => moveProgressBar()}>
              <div className="progress__wrap progress progress__limit" key={idbar} progress={progress}>
                <div className="progress__bar progress"></div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  )

}

export default SkillsColumnTwo;